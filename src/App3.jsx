import './App3.css'
import {useState } from 'react' ;

function App(){
    return (
        <p>
            <Bloque/>
            <AlertPrimary nombre ="Suena ya"/>
            <AlertSecondary>
                <li className="text-danger text-center"> Nombre: Maria</li>
                <li className="text-info text-end"> Nombre: Juana</li>
            </AlertSecondary>
        </p>
    )

}

function Bloque() {
    return (
      <div className="Bloque">
        <header 
        className="App-header text-center py-4">
          <h1>Hello CAR 2022</h1>
        </header>
      </div>
    )
}


function AlertPrimary(props){
    return (
        <div className="alert alert-primary px-4" role="alert" >
            Alert 1 {props.nombre}
        </div>
    )
}

function AlertSecondary(props){
    return (
        <div className="alert alert-secondary px-4" role="alert" >
            Alert 1 {props.children}
            
        </div>
    )
}

export default App