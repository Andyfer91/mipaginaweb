import {Emojiitem} from './emojiitem';

export function Emojilist ({datos}) {
    let renderEmojis = datos.map((emoji) =>{
        return (
        <div className="col-4 py-2">
            <Emojiitem 
                title={emoji.title} 
                symbol={emoji.symbol} 
                keywords={emoji.keywords}
            />
        </div>
        )
    })
    return (
    <div className='row d-flex  text-center py-5'>
        {renderEmojis}
{/*         <div className="col-4">
            <Emojiitem title='100' symbol='💯' keywords='"hundred points symbol symbol wow wow win win perfect perfect parties parties"'/>
        </div>
        <div className="col-4">
            <Emojiitem title='100' symbol='100' keywords='100'/>
        </div>
        <div className="col-4">
            <Emojiitem title='100' symbol='100' keywords='100'/>
        </div> */}
    </div>
    );
}