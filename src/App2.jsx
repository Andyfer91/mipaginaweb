import './App2.css'
import {useState } from 'react' ;

function Bloque() {
  return (
    <div className="Bloque">
      <header 
      className="App-header"
      onClick={()=> {
        // Forma 2 de utilizar onClick, crear el evento en la logica interna
        console.log("Click al header");
}}      
>
        <h1>Hello CAR 2022</h1>
        <p>
          Este es un texto absurdo.
          impsum lorem dolor, sed diam 
        </p>
        
      </header>
    </div>
  )
}
// Forma 1 de utilizar evento onClick, crear una funcion y llamarla
function clickAlTitulo() {
  alert('CLick al titulo');
}

function App(){
  const [text, setTexto] = useState("hola mundo!");
  const [mostrar, setMostrar] = useState(false);

  return (
    <div className="App">
        <input type="text"
        //Placeholder coloca un texto en background
        placeholder="Ingrese su texto"
        onChange={(evento) => {
          setTexto(evento.target.value);
        }}/>
        {/* agregar un boton de ayuda */}
        <button onClick={() => {
          setTexto ('Javascript');
        }}>Mostrar
        </button>

        <p>{mostrar ? text : 'esto es una prueba'}</p>


      <header 
      className="App-header" >
        <p>
          <Bloque/>
          <Bloque/>
          <Bloque/>
          <Bloque/>
        </p>
        
      </header>


      <div>
        <h1 
        className="Titulo-categoria"
        onClick ={clickAlTitulo}
        >Categorias
        </h1>
        <Bloque/>


      </div>
      <footer>
        El dia de la marmota
        <p>{text}</p>
      </footer>
    </div>
  )
}



export default App2
