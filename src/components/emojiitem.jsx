export function Emojiitem ({title, symbol, keywords}) {
    return (
        <div className="card">
            <h5 className="card-header">{title}</h5>
            <div className="card-body">
                <h2 className="card-title">{symbol}</h2>
                <div className="card-text text-muted">{keywords}</div>
            </div>
    </div>
    )
}