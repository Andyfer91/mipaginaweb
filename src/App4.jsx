import './App.css'

import {TituloEncabezado} from '/src/components/Titulo';
import {Buscador} from '/src/components/Buscador';
import {Emojilist} from '/src/components/emojilist';

import datos from './database/emoji-list.json';


function App () {
    return (
        <div className='container py-4'>
            <TituloEncabezado/>
            <Buscador/>
            <Emojilist datos={datos.emojis}/>
        </div>
    );
}

//export default App;