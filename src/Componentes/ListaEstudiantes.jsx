import {EstudianteItem} from './EstudianteItem.jsx';

export function ListaEstudiantes ({datos}) {
    let renderEstudiante = datos.map((info) =>{
        return (
        <div className='container py-5'>
        <div className="row justify-content-md-center">
        <div className="col-7 py-2">
            <EstudianteItem 
                nombre_completo={info.nombre_completo} 
                profesion={info.profesion} 
                descripcion={info.descripcion}
                correo = {info.correo}
                foto = {info.foto}
            />
        </div>
        </div>
        </div>
        )
    })
    return (
    <div className='row d-flex  text-center py-5'>
        {renderEstudiante}
    </div>
    );
}