import {ListaEstudiantes} from './Componentes/ListaEstudiantes.jsx';

import datos from './database/about.json';

function App () {
    return (
        <div>
            <ListaEstudiantes datos={datos.info}/>
        </div>
    );
}

export default App;